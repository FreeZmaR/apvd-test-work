<?php


namespace App\Http\Contracts;


interface PresenterContract
{
    public static function present($item);
    public static function presentArray(array $items): array;
}
