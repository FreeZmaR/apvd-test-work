<?php


namespace App\Http\Contracts\Services;


interface CurrencyServiceContract
{
    public function getCurrencyList(bool $first = false): array;
}
