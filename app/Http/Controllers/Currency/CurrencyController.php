<?php


namespace App\Http\Controllers\Currency;


use App\Http\Contracts\Services\CurrencyServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Presenters\Currency\CurrencyListPresenter;

class CurrencyController extends Controller
{
    public function currencyList()
    {
        $result = resolve(CurrencyServiceContract::class)->getCurrencyList(true);

        return view('index', [
            'change' => $result['change'],
            'data' => CurrencyListPresenter::presentArray($result['data']['stock'] ?? []),
        ]);
    }

    public function refreshList()
    {
        $result = resolve(CurrencyServiceContract::class)->getCurrencyList();

        return response()->json([[
            'change' => $result['change'] ?? false,
            'data' => CurrencyListPresenter::presentArray($result['data']['stock'] ?? []),
        ]]);
    }
}
