<?php


namespace App\Http\Presenters;


use App\Http\Contracts\PresenterContract;

abstract class BasePresenter implements PresenterContract
{
    abstract protected static function item($item);

    public static function present($item)
    {
        return static::item($item);
    }

    public static function presentArray(array $items): array
    {
        return array_map(function ($item) {
            return static::item($item);
        }, $items);
    }
}
