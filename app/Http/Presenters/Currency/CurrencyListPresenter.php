<?php


namespace App\Http\Presenters\Currency;


use App\Http\Presenters\BasePresenter;

class CurrencyListPresenter extends BasePresenter
{

    protected static function item($item)
    {
        return [
            'name' => $item['name'] ?? '',
            'amount' => self::formatNumber($item['price']['amount'], 2) ?? 0.00,
            'volume' => self::formatNumber($item['volume']) ?? 0,
        ];
    }

    private static function formatNumber($number, $decimals = 0)
    {
        $number = abs($number);

        return is_numeric($number) ? number_format($number, $decimals, ",", " ") : null;
    }
}
