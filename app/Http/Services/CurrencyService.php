<?php


namespace App\Http\Services;


use App\Http\Contracts\Services\CurrencyServiceContract;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class CurrencyService implements CurrencyServiceContract
{

    public function getCurrencyList(bool $first = false): array
    {
        $response = Http::get(config('currency.fetch_uri'));
        $data = $response->json();

        if ($first) {
            return [
                'change' => false,
                'data' => $data,
            ];
        }

        $isChange = $this->_isChangingList($data);

        return [
            'change' => !$isChange,
            'data' => $isChange ? $data : [],
        ];
    }

    private function _isChangingList(array $list): bool
    {
        $cache = Cache::get('list');

        if (!$cache) {
            Cache::put('list', serialize($list));
            return false;
        }

        return $cache == serialize($list);
    }
}
