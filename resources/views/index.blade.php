<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>APVD test work</title>
</head>
<body>
    <h1>Test page</h1>
    <div class="container">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Amount</th>
                <th scope="col">Volume</th>
            </tr>
            <button id="refresh_button" type="button" class="btn btn-primary btn-sm">Refresh</button>
            </thead>
            <tbody id="table_body">
            @foreach($data as $item)
                <tr>
                    <td>{{ $item['name'] }}</td>
                    <td>{{ $item['amount'] }}</td>
                    <td>{{ $item['volume'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>
    let _isLoading = false;
    let _timeToRefresh = 15;

    const refreshButton = $('#refresh_button').on('click', event => {
        _timeToRefresh = 15;
        _fetchData();
    });
    const tableBody = $('#table_body');

    function _fetchData() {
        if (_isLoading) {
            return;
        }

        _isLoading = true;

        fetch('{{ route('refresh') }}')
            .then(response => response.json())
            .then(response => _appendRows(response))
            .catch(error => console.log(error))
            .finally(() => {
                _isLoading = false;
                _timeToRefresh = 15;
            });
    }

    function _appendRows(response) {
        if (!response[0].change) {
            return;
        }

        $(tableBody).html(response[0].data.map(_rowString).join(''));
    }

    function _rowString(data) {
        return `<tr><td>${data.name}</td><td>${data.amount}</td><td>${data.volume}</td></tr>`;
    }

    setInterval(() => {
        if (!_timeToRefresh) {
            _fetchData();
        }
        _timeToRefresh -= 1;
    }, 1000);


</script>
</html>
